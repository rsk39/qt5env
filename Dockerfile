# syntax=docker/dockerfile:1
ARG TARGET_PLATFORM=linux/arm/v7
ARG TARGET_DEVICE

FROM --platform=${TARGET_PLATFORM} debian:buster-slim as sysroot-builder
RUN apt-get update && apt-get install -y --no-install-recommends \
    libicu-dev \
    libmariadbclient-dev \
    libmariadb-dev-compat \
    libssl-dev \
  && rm -rf /var/lib/apt/lists/*

FROM --platform=${BUILDPLATFORM} debian:buster-slim as builder
ARG TARGET_PLATFORM
ENV TARGET_PLATFORM=${TARGET_PLATFORM}
ARG TARGET_DEVICE
ENV TARGET_DEVICE=${TARGET_DEVICE:-rpi3}
ENV QT_DIR=/qt5
ENV QT_BIN_PATH=${QT_DIR}/linaro
ENV SYSROOT_DIR=/sysroot
ENV TOOLCHAIN_DIR=${QT_DIR}/toolchain
ENV TOOLCHAIN_VERSION_MINOR=7.4-2019.02
ENV TOOLCHAIN_VERSION_RELEASE=7.4.1-2019.02
ENV TOOLCHAIN_ARCH=arm-linux-gnueabihf
ENV TOOLCHAIN_NAME=gcc-linaro-${TOOLCHAIN_VERSION_RELEASE}-x86_64_${TOOLCHAIN_ARCH}
ENV QT_MKSPEC=linux-arm-gnueabihf-g++
ENV PATH=${QT_BIN_PATH}/bin:$PATH
COPY --from=sysroot-builder /lib ${SYSROOT_DIR}/lib
COPY --from=sysroot-builder /usr/include ${SYSROOT_DIR}/usr/include
COPY --from=sysroot-builder /usr/lib ${SYSROOT_DIR}/usr/lib

WORKDIR ${QT_DIR}
RUN set -eux; \
    apt-get update; \
    apt-get install -y --no-install-recommends \
        bison \
        build-essential \
        ca-certificates \
        curl \
        gcc \
        gperf \
        pkg-config \
        python3 \
    ; \
    \
# rebuild symlinks after copy from sysroot-builder
    curl -SLO https://raw.githubusercontent.com/riscv/riscv-poky/master/scripts/sysroot-relativelinks.py; \
    python3 sysroot-relativelinks.py ${SYSROOT_DIR}; \
    rm sysroot-relativelinks.py; \
    \
# download arm toolchain
    mkdir -p ${TOOLCHAIN_DIR}; \
    cd ${TOOLCHAIN_DIR}; \
    curl -SLO https://releases.linaro.org/components/toolchain/binaries/${TOOLCHAIN_VERSION_MINOR}/${TOOLCHAIN_ARCH}/${TOOLCHAIN_NAME}.tar.xz; \
    tar -xvf ${TOOLCHAIN_NAME}.tar.xz; \
    rm ${TOOLCHAIN_NAME}.tar.xz; \
    \
# download Qt5 sources
    cd ${QT_DIR}; \
    curl -SL -O https://download.qt.io/official_releases/qt/5.15/5.15.2/submodules/qtbase-everywhere-src-5.15.2.tar.xz \
             -O https://download.qt.io/official_releases/qt/5.15/5.15.2/submodules/qtserialport-everywhere-src-5.15.2.tar.xz \
             -O https://download.qt.io/official_releases/qt/5.15/5.15.2/submodules/qtserialbus-everywhere-src-5.15.2.tar.xz \
             -O https://download.qt.io/official_releases/qt/5.15/5.15.2/submodules/qtwebsockets-everywhere-src-5.15.2.tar.xz \
    ; \
    tar -xvf qtbase-everywhere-src-5.15.2.tar.xz; \
    tar -xvf qtserialport-everywhere-src-5.15.2.tar.xz; \
    tar -xvf qtserialbus-everywhere-src-5.15.2.tar.xz; \
    tar -xvf qtwebsockets-everywhere-src-5.15.2.tar.xz; \
    rm qt*.tar.xz; \
# create mkspec for armhf devices
    cp -R qtbase-everywhere-src-5.15.2/mkspecs/linux-arm-gnueabi-g++ \
          qtbase-everywhere-src-5.15.2/mkspecs/${QT_MKSPEC} \
    ; \
    sed -i -e 's/arm-linux-gnueabi-/${TOOLCHAIN_ARCH}-/g' qtbase-everywhere-src-5.15.2/mkspecs/${QT_MKSPEC}/qmake.conf; \
    \
# build qmake
    cd ${QT_DIR}/qtbase-everywhere-src-5.15.2; \
    mkspec_device=; \
    case "${TARGET_DEVICE}" in \
        'rpi3') \
          mkspec_device='linux-rasp-pi3-g++' ;; \
        'rpi4') \
          mkspec_device='linux-rasp-pi4-v3d-g++' ;; \
        'tinkerboard') \
          mkspec_device='linux-tinkerboard-g++' ;; \
        *) \
          echo >&2 "error: unsupported mkspec device '${TARGET_DEVICE}'"; exit 1 ;; \
    esac; \
    if [ -z "$mkspec_device" ]; then \
        echo >&2 "using base config ('linux-rasp-pi3-g++')..."; \
        mkspec_device='linux-rasp-pi3-g++'; \
    fi; \
    \
    mkdir ${QT_BIN_PATH}; \
    mkdir ${SYSROOT_DIR}/mysql; \
    ln -s ${SYSROOT_DIR}/usr/include/mysql ${SYSROOT_DIR}/mysql/include; \
    ./configure -v \
        -recheck-all \
        -release \
        -opensource \
        -confirm-license \
        -make libs \
        -ssl \
        -qt-pcre \
        -no-dbus \
        -no-evdev \
        -no-feature-accessibility \
        -no-feature-cssparser \
        -no-feature-sessionmanager \
        -no-feature-texthtmlparser \
        -no-feature-textmarkdownreader \
        -no-feature-textmarkdownwriter \
        -no-feature-textodfwriter \
        -no-freetype \
        -no-gui \
        -no-gif \
        -no-glib \
        -no-harfbuzz \
        -no-ico \
        -no-linuxfb \
        -no-libjpeg \
        -no-libpng \
        -no-opengl \
        -no-sql-sqlite \
        -no-widgets \
        -no-zlib \
        -nomake examples \
        -no-compile-examples \
        -nomake tests \        
        -device $mkspec_device \
        -device-option CROSS_COMPILE=${TOOLCHAIN_DIR}/${TOOLCHAIN_NAME}/bin/${TOOLCHAIN_ARCH}- \
        -sysroot ${SYSROOT_DIR} \
        -no-use-gold-linker \
        -prefix /usr/local/qt5 \
        -extprefix ${QT_BIN_PATH} \
        MYSQL_PREFIX=${SYSROOT_DIR}/mysql \
        -sql-mysql; \
    make -j4 && make install; \
# build qtserialport module
    cd ${QT_DIR}/qtserialport-everywhere-src-5.15.2; \
    qmake && make -j4 && make install; \
# build qtserialbus module
    cd ${QT_DIR}/qtserialbus-everywhere-src-5.15.2; \
    qmake && make -j4 && make install; \
# build qtwebsockets module
    cd ${QT_DIR}/qtwebsockets-everywhere-src-5.15.2; \
    qmake && make -j4 && make install; \
    \
# clearing up to save space
    cd ${QT_DIR}; \
    rm -rf qt*src*; \
    packagesToSave="make"; \
    apt-mark auto '.*' > /dev/null; \
    apt-mark manual $packagesToSave; \
    apt-get purge -y --auto-remove; \
    rm -rf /var/cache/apt/* /var/lib/apt/lists/*

CMD ["qmake", "-v"]
