#!/bin/sh
# a copy of https://doc.qt.io/qt-5/linux-deployment.html#creating-the-application-package script
appname=`basename $0 | sed s,\.sh$,,`

dirname=`dirname $0`
tmp="${dirname#?}"

if [ "${dirname%$tmp}" != "/" ]; then
  dirname=$PWD/$dirname
fi
LD_LIBRARY_PATH=$dirname
export LD_LIBRARY_PATH
#echo $LD_LIBRARY_PATH
#echo $(ldd $dirname/$appname)
$dirname/$appname "$@"
