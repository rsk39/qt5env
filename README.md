# Docker image for building Qt5 environment
![Qt](https://upload.wikimedia.org/wikipedia/commons/thumb/0/0b/Qt_logo_2016.svg/168px-Qt_logo_2016.svg.png) ![Linaro](https://upload.wikimedia.org/wikipedia/commons/thumb/5/5a/Logo_linaro.png/220px-Logo_linaro.png)

Based on [Qt 5.15.2 LTS](https://www.qt.io/blog/qt-5.15-released) open-source (LGPLv3) release version.

Qt Modules included:

* **no `gui`**
* `network`
* `websockets`
* `serialbus`
* `serialport`
* `sql`

Uses [Linaro Toolchain](https://releases.linaro.org/) to build up ARMv7 (armv7l) cross compile environment.

## Installation
Just use Docker Hub to get ready-to-use environment:
```bash
docker pull rsk39/qt5-env:<TARGET_DEVICE>
```
where `TARGET_DEVICE`s are:

* `amd64`
* `rpi3`
* `rpi4`
* `tinkerboard`

To create a new environment for different device with another cross-compile toolchain:

* install Docker Buildx <https://docs.docker.com/buildx/working-with-buildx/#build-with-buildx>
* check multi-arch support is enabled <https://docs.docker.com/desktop/multi-arch/>
* specify `TARGET_DEVICE` as a build-arg and image version tag
* provide `TARGET_PLATFORM` build-arg. Use [Docker Buildx arch format](https://docs.docker.com/desktop/multi-arch/) ('\-\-platform' parameter)
* update Dockerfile's environment variables `TOOLCHAIN_VERSION_MINOR`, `TOOLCHAIN_VERSION_RELEASE`, `TOOLCHAIN_ARCH` from <https://releases.linaro.org/components/toolchain/binaries/>
* update Dockerfile's environment variable `QT_MKSPEC` according to <https://code.qt.io/cgit/qt/qtbase.git/tree/mkspecs?h=5.15.2>
* add necessary Qt mkspec_device at `#build qmake` stage according to <https://code.qt.io/cgit/qt/qtbase.git/tree/mkspecs/devices?h=5.15.2>

Then run this command to rebuild an image locally:
```bash
docker build --build-arg TARGET_PLATFORM=<platfrom> --build-arg TARGET_DEVICE=<device> -t rsk39/qt5-env:<device> . --load
```
Or push the result to Docker Hub right after the build would be completed:
```bash
docker build --build-arg TARGET_PLATFORM=<platfrom> --build-arg TARGET_DEVICE=<device> -t rsk39/qt5-env:<device> . --push
```

## Usage
```bash
docker run -it -v "$(pwd)"/app:/app rsk39/qt5-env bash
```
will get you into the bash shell with your source files located at "/app".

Or use something like this to start your own command as one-liner:
```bash
docker run -it --rm -v "$(pwd)"/app:/app rsk39/qt5-env <command>
```
